# GitLab Performance Test Framework - Data

This Project serves as a [LFS](https://docs.gitlab.com/ee/workflow/lfs/manage_large_binaries_with_git_lfs.html) data repository for the [GitLab Performance Tool](https://gitlab.com/gitlab-org/quality/performance). It stores sanitized projects for importing into environments for performance testing.

Currently you'll find the following for use with the Tool:
* [`gitlabhq_export.tar.gz`](projects_export/gitlabhq_export.tar.gz) - An archive of the `gitlabhq` project that can be imported to GitLab. This is the main project used for performance testing and is itself a sanitized backup of the `gitlab-ce` project (via our GitHub backup).
* [`linux_export.tar.gz`](projects_export/linux_export.tar.gz) - An archive of the `Linux` project with that can be imported to GitLab. This project is populated with GitLab data: merge requests, issues, labels. 

Additionally this project provides [tools to generate data](docs/README.md) for GitLab projects.
