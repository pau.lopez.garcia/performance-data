# Project Tarball Generator Tool

Tarball Generator Tool duplicates existing data of the specified project export tarball to inflate the number of merge requests and issues.

The full options for running the tool can be seen by getting the help output by running `bin/inflate-project-tarball --help`:

```
Inflate an existing GitLab Project tarball by duplicating MRs and Issues.
Usage: inflate-project-tarball [options]

Options:
  --project-tarball=<s>    Location of project tarball to import. Can be local or remote. (Default:
                           ./performance-data/gitlabhq_export.tar.gz)
  --merge-requests=<i>     Number of new merge requests to add (default: 20000)
  -i, --issues=<i>         Number of issues to add (default: 10000)
  -h, --help               Show help message

Examples:
  ./bin/inflate-project-tarball --project-tarball ./performance-data/projects_export/gitlabhq_export.tar.gz --merge-requests 20_000 --issues 10_000
```
